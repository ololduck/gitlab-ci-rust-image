FROM rust:latest
RUN rustup --verbose toolchain install stable beta nightly --profile minimal --component clippy rustfmt && \
    rustup default stable && \
    cargo install --color=always cargo-binstall && \
    cargo binstall --no-confirm cargo-quickinstall && \
    cargo binstall --no-confirm gitlab-report cargo-audit cargo-toml-lint cargo-cache cargo-rdme cargo-outdated cargo-semver-checks sccache

ENV SCCACHE_DIR=/usr/local/cargo/sccache \
    SCCACHE_CACHE_SIZE=1G
VOLUME "/usr/local/cargo/registry"  "${SCCACHE_DIR}"
ENV RUSTC_WRAPPER=$CARGO_HOME/bin/sccache